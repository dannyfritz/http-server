use std::collections::HashMap;

#[macro_use] extern crate nickel;
use nickel::{Nickel, StaticFilesHandler, HttpRouter};

extern crate rustc_serialize;

#[derive(RustcDecodable, RustcEncodable, Debug)]
struct Person {
    name: String,
    age: i32,
}

fn main() {
    let mut server = Nickel::new();

    let mut people = vec![
        Person { name: "Danny".into(), age: 27 }
    ];

    server.utilize(StaticFilesHandler::new("assets/public/"));

    server.get("/", middleware! { |_request, response|
        println!("{:?}", people);
        let mut data = HashMap::new();
        data.insert("people", &people);
        return response.render("assets/views/index.hbs", &data);
    });

    server.get("/:name", middleware! { |request, response|
        let mut data = HashMap::new();
        data.insert("name", request.param("name"));
        return response.render("assets/views/index.hbs", &data);
    });

    server.listen("127.0.0.1:6767");
}
